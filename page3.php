<?php

include 'header.php';

if (isset($_GET['pageno'])) {
	$pageno = $_GET['pageno'];
} 
else {
	$pageno = 1;
}
$no_of_records_per_page = 6; //количество элементов на одной странице
$offset = ($pageno-1) * $no_of_records_per_page; //сдвиг

$conn=mysqli_connect("std-mysql","std_819","12345678","std_819");
// Check connection
if (mysqli_connect_errno()){
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
	die();
}
					
$total_pages_sql = "SELECT COUNT(*) FROM Profiles";
$result = mysqli_query($conn,$total_pages_sql);
$total_rows = mysqli_fetch_array($result)[0];
$total_pages = ceil($total_rows / $no_of_records_per_page);

?>
			
<div class="row">
	<div class="col-2"></div>
	<div class="col-8">
	<h1>Дни рождения</h1>
	<p>Список дней рождения членов большого Клуба ветеранов Морского космического флота  (МКФ)</p>
	<p>Страница <?=$pageno?> из <?=$total_pages?></p>
		<?php
		
		$sql = "SELECT * FROM Profiles LIMIT $offset, $no_of_records_per_page";
		$res_data = mysqli_query($conn,$sql);
		?>
			<table class="table table-striped table-bordered">
			<thead>
			<tr>
				<th>Дата рождения</th>
				<th>Фамилия, имя, отчество члена Клуба ветеранов МКФ</th>
				<th>Ходил(а) в рейсы в составе экипажа, экспедиции НИС</th>
				<th>Примечание (юбилей)</th>
				<th>Отделение Клуба (город)</th>
			</tr>
			</thead>
			<tbody>
						
			<?php
			while($row = mysqli_fetch_array($res_data)){
			?>
				<tr>
					<td> <?=$dob = date("d.m.Y", strtotime($row[birthday]))?></td> 
					<td><b><a href="/profile.php?id=<?=$row[id]?>"><?=$row[last_name]?></b><br><?=$row[first_name]?> <?=$row[patronymic]?></a></td>
					<td><?=$row[description]?></td>
					<td><?php //считаем юбилей
						$today = new DateTime();
						$birthdate = new DateTime($row[birthday]);
						$interval = $today->diff($birthdate);
						
						$age = (int) $interval->format('%y');
						
						if(($age%5)==0){
							echo 'Юбилей '.$age;
						}
					?></td>
					<td><?=$row[grade]?></td>
				</tr>
				<?php
				}
				?>
			</tbody>
			</table>
		<?php
		mysqli_close($conn);
		?>

	</div>
	<div class="col-2"></div>
</div>
			
<div class="row">
	<div class="col-2"></div> <!--Пагинация-->
		<div class="col-2"><a href="?pageno=1" class="btn btn-light">Первая</a></div>
		<div class="col-2">
			<a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>" class="btn btn-light">Предыдущая</a>
		</div>
		<div class="col-2">
			<a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>" class="btn btn-light">Следующая</a>
		</div>
		<div class="col-2"><a href="?pageno=<?php echo $total_pages; ?>" class="btn btn-light">Последняя</a></div>
	
	<div class="col-2"></div>
</div>
			
			
<?php

include 'footer.php';

?>