<?php

include 'header.php';

if (isset($_GET['pageno'])) {
	$pageno = $_GET['pageno'];
} 
else {
	$pageno = 1;
}
$no_of_records_per_page = 6; //количество элементов на одной странице
$offset = ($pageno-1) * $no_of_records_per_page; //сдвиг

$conn=mysqli_connect("std-mysql","std_819","12345678","std_819");
// Check connection
if (mysqli_connect_errno()){
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
	die();
}
					
$total_pages_sql = "SELECT COUNT(*) FROM Profiles";
$result = mysqli_query($conn,$total_pages_sql);
$total_rows = mysqli_fetch_array($result)[0];
$total_pages = ceil($total_rows / $no_of_records_per_page);
?>
			
<div class="row">
		
	<div class="col-2"></div>
	<div class="col-8">
				
		<h1>Руководители</h1>
		<p>Список руководителей, ходивших в   экспедиционные рейсы  в должности начальника экспедиции (1963-1994гг.)</p>
		<p>Страница <?=$pageno?> из <?=$total_pages?></p>
		<?php
					
			$sql = "SELECT * FROM Profiles LIMIT $offset, $no_of_records_per_page";
			$res_data = mysqli_query($conn,$sql);
		?>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Фамилия, инициалы</th>
					<th>Перечень судов, № эксп.рейса,кот.возгл.</th>
				</tr>
			</thead>
			<tbody>
							
		<?php
			while($row = mysqli_fetch_array($res_data)){
		?>
				<tr>
					<td><b><a href="/profile.php?id=<?=$row[id]?>"><?=$row[last_name]?></b><br><?=$row[first_name]?> <?=$row[patronymic]?></a></td> 
					<td><?=$row[description]?></td>
				</tr>
		<?php
			}
		mysqli_close($conn);
		?>
			</tbody>
		</table>
					
	</div>
	<div class="col-2"></div>
</div>
			
<div class="row">
	<div class="col-2"></div>
	<div class="col-2"><a href="?pageno=1" class="btn btn-light">Первая</a></div>
	<div class="col-2">
		<a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>" class="btn btn-light">Предыдущая</a>
	</div>
	<div class="col-2">
		<a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>" class="btn btn-light">Следующая</a>
	</div>
	<div class="col-2"><a href="?pageno=<?php echo $total_pages; ?>" class="btn btn-light">Последняя</a></div>
				
	<div class="col-2"></div>
</div>			
			
			
<?php

include 'footer.php';

?>